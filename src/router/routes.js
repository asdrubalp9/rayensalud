const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { name: '', path: '', component: () => import("pages/Login.vue"), meta: { requireLogin: false } },
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { name: '', path: '', component: () => import("pages/Login.vue"), meta: { requireLogin: false } },
    ]
  },
  {
    path: '/signup',
    name: 'signup',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { name: '', path: '', component: () => import("pages/SignUp.vue"), meta: { requireLogin: false } },
    ]
  },
  {
    path: '/explore',
    name: 'explore',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { name: '', path: '', component: () => import("pages/Explore.vue"), meta: { requireLogin: false } },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
