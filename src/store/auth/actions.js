import { api } from 'boot/axios'
import { Notify, date } from 'quasar'

export const refreshToken = async ({ commit, dispatch }, payload) => {
  console.log('refreshing')
  await api.post('refresh')
}

export const doRegisterUser = async ({ commit, dispatch }, payload) => {
  await api.post('registro', payload)
    .then(response => {
      const token = response.data
      commit('setToken', token)
      api.defaults.headers.common.Authorization = 'bearer ' + token.access_token
    })
}

export const doLogin = async ({ commit, dispatch }, payload) => {
  await api.post('/api/usuarios/login', {...payload})
    .then(response => {
      const token = response.data
      commit('setToken', token)
      api.defaults.headers.common.Authorization = 'bearer ' + token.access_token
      commit('setUserData', response.data.data.user)
      return response
    }).catch(function (e) {
      Notify.create({
        message: `<p class="q-ma-none">${e.response.data.message}</p>`,
        color: 'red',
        position: 'center',
        multiLine: true,
        html: true,
        type: 'negative',
        icon: 'fas fa-exclamation-triangle'
      })
      commit('removeToken')
    })
}

export const signOut = ({ commit }) => {
  api.post('logout').then(r => {
    api.defaults.headers.common.Authorization = ''
    commit('removeToken')
  })
}

export const getMe = async ({ commit }, token) => {
  await api.get('users/me', token.access).then(response => {
    commit('setMe', response.data)
  })
}
// export const getExpireData = ({ commit, dispatch }) => {
// }
export const init = async ({ commit, dispatch }) => {
  let token = localStorage.getItem('token')
  const fecha = localStorage.getItem('signinAt')
  try {
    const signinAt = new Date(fecha.split('.')[0])
    const expiresIn = localStorage.getItem('expiresIn')
    const hoursToForceExpire = (expiresIn / 60) / 60
    const currentDate = new Date()
    const diff = date.getDateDiff(currentDate, signinAt, 'hours')
    if (diff >= hoursToForceExpire) {
      token = ''
      localStorage.clear()
    }
    // revisar si expiro el token para anularlo
    console.log('diferencia horas token', diff)
    if (token) {
      await commit('setToken', JSON.parse(token))
      api.defaults.headers.common.Authorization = 'bearer ' + JSON.parse(token).access_token
    } else {
      commit('removeToken')
    }
  } catch (error) {
    commit('removeToken')
  }
}
