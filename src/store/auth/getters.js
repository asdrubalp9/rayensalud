export function getMe (state) {
  return JSON.parse(window.localStorage.getItem('userData'))
  // return state.user
}
export function getToken (state) {
  return state.token
}
export function isAuthenticated (state) {
  return state.isAuthenticated
}
