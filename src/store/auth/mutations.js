export const setToken = (state, token) => {
  state.token = token
  state.isAuthenticated = true
  window.localStorage.setItem('token', JSON.stringify(token))
}

export const setExpireData = (state, token) => {
  state.signinAt = new Date()
  state.expiresIn = token.expires_in
  window.localStorage.setItem('signinAt', state.signinAt)
  window.localStorage.setItem('expiresIn', JSON.stringify(state.expiresIn))
}

export const removeToken = (state, token) => {
  console.log('removing token')
  state.token = ''
  state.isAuthenticated = false
  window.localStorage.removeItem('userData')
  window.localStorage.clear()
}

export const setMe = (state, me) => {
  state.user = me
}

export const setUserData = (state, data) => {
  state.user = data
  window.localStorage.setItem('userData', JSON.stringify(state.user))
}
export const setCategories = (state, data) => {
  if (data.categorias) {
    state.categorias = data.categorias
    window.localStorage.setItem('categorias', JSON.stringify(state.categorias))
  }
}

export const setEstados = (state, data) => {
  if (data.estados) {
    state.estados = data.estados
    window.localStorage.setItem('estados', JSON.stringify(state.estados))
  }
}
export const setRemitente = (state, data) => {
  if (data.remitente) {
    state.remitente = data.remitente
  }
}
