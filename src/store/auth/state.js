export default function () {
  return {
    user: {},
    signinAt: '',
    expiresIn: '',
    token: '',
    isAuthenticated: false
  }
}
